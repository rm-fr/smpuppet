module smpuppet

go 1.14

require (
	github.com/asaskevich/EventBus v0.0.0-20200907212545-49d423059eef // indirect
	github.com/flashmob/go-guerrilla v1.6.1
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/sirupsen/logrus v1.7.0 // indirect
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
)
