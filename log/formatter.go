package log

import (
	"bytes"
	"fmt"
	"strings"
	"time"
)

// Formatter is the interface for output formatting of log messages.
type Formatter interface {
	Format(Level, Pairs, string) ([]byte, error)
}

const DefaultTimeFormat = "2006/01/02 15:04:05.999999 (-07:00)"

// TimeFormatter formats the message by prepending time to the log message.
type TimeFormatter struct {
	layout string
}

func NewTimeFormatter(format string) *TimeFormatter {
	return &TimeFormatter{
		layout: format,
	}
}

func (f *TimeFormatter) Format(lvl Level, fields Pairs, msg string) ([]byte, error) {
	var segments = make([]string, 0)

	segments = append(segments, time.Now().Format(f.layout), lvl.String())
	for p := range fields {
		segments = append(segments, fmt.Sprintf("[%s:%v]", p.Key, p.Value))
	}
	segments = append(segments, msg)

	str := strings.TrimSpace(strings.Join(segments, " "))
	return bytes.NewBufferString(fmt.Sprintln(str)).Bytes(), nil
}
