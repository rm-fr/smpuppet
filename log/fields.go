package log

type Pair struct {
	Key   string
	Value interface{}
}

type OrderT func([]string) []string
type Pairs <-chan Pair

type Mapper interface {
	Set(key string, val interface{})
	Len() int
	Copy() Mapper
	Iter(OrderT) Pairs
}

// DefaultMap implements Mapper.
type DefaultMap struct {
	pairs map[string]struct {
		idx uint
		val interface{}
	}
	keys []string
}

func NewMap() *DefaultMap {
	return &DefaultMap{
		pairs: make(map[string]struct {
			idx uint
			val interface{}
		}),
		keys: make([]string, 0),
	}
}

func (m *DefaultMap) Copy() Mapper {
	keys := m.Keys()
	pairs := make(map[string]struct {
		idx uint
		val interface{}
	}, len(keys))

	for k, v := range m.pairs {
		pairs[k] = struct {
			idx uint
			val interface{}
		}{
			idx: v.idx,
			val: v.val,
		}
	}

	return &DefaultMap{
		pairs: pairs,
		keys:  keys,
	}
}

func (m *DefaultMap) Len() int {
	return len(m.keys)
}

// Set updates or inserts the provided key-value pair, updating the key age.
func (m *DefaultMap) Set(key string, val interface{}) {
	idx := len(m.pairs)
	if v, ok := m.pairs[key]; ok {
		idx--
		m.keys = append(m.keys[:v.idx], m.keys[v.idx+1:]...)
	}
	m.pairs[key] = struct {
		idx uint
		val interface{}
	}{
		idx: uint(idx),
		val: val,
	}
	m.keys = append(m.keys, key)
}

// Keys returns a copy of the map keys, in order of last update.
func (m *DefaultMap) Keys() []string {
	cp := make([]string, len(m.keys))
	copy(cp, m.keys)
	return cp
}

// Iter provides an iterator over the map pairs, in the order specified by ordF.
// ordF takes the map keys in order of last update ascending, and returns
// (a subset of) them in the order to be returned.
// The keys filtered by ordF are not returned.
//
// Usage:
// keyOrder := func(k []string) []string {
// 	sort.Sort(sort.Reverse(sort.StringSlice(k))); return []string(k)
// }
// for p := range map.Iter(keyOrder) { fmt.Printf("%s:%s\n", p.Key, p.Value) }
func (m *DefaultMap) Iter(ordF OrderT) Pairs {
	ch := make(chan Pair)
	keys := m.Keys()
	if ordF != nil {
		keys = ordF(keys)
	}
	go func(ch chan<- Pair) {
		for _, key := range keys {
			ch <- Pair{
				Key:   key,
				Value: m.pairs[key].val,
			}
		}
		close(ch)
	}(ch)

	return ch
}
