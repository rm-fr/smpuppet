package log

import (
	"fmt"
	"io"
	"os"
)

var (
	DefaultOutput    io.Writer = os.Stderr
	DefaultFormatter Formatter = NewTimeFormatter(DefaultTimeFormat)
	DefaultFatalFunc func()    = func() { os.Exit(1) }
)

// StandardLogger is the default implementation of Logger.
type StandardLogger struct {
	write func([]byte)

	lvl Level

	fields Mapper
	f      Formatter

	fatalFunc func()
}

// NewStdLogger returns a new StandardLogger,
// logging to the provided output.
func NewStdLogger(opts ...func(*StandardLogger)) *StandardLogger {
	// Default level is info
	l := &StandardLogger{
		write:     createWriter(DefaultOutput),
		fields:    NewMap(),
		f:         DefaultFormatter,
		fatalFunc: DefaultFatalFunc,
	}

	for _, opt := range opts {
		opt(l)
	}

	return l
}

func WithStdLevel(lvl Level) func(*StandardLogger) {
	return func(l *StandardLogger) {
		l.lvl = lvl
	}
}

func WithStdFormatter(f Formatter) func(*StandardLogger) {
	return func(l *StandardLogger) {
		l.f = f
	}
}

func WithStdOutput(w io.Writer) func(*StandardLogger) {
	return func(l *StandardLogger) {
		l.write = createWriter(w)
	}
}

func WithStdFatalFunc(fatalF func()) func(*StandardLogger) {
	return func(l *StandardLogger) {
		l.fatalFunc = fatalF
	}
}

func createWriter(out io.Writer) func([]byte) {
	o := NewLockWriter(out)

	return func(output []byte) {
		o.Write(output)
	}
}

func (l *StandardLogger) With(key string, val interface{}) Logger {
	newFields := l.fields.Copy()
	newFields.Set(key, val)

	return &StandardLogger{
		write:  l.write,
		lvl:    l.lvl,
		fields: newFields,
		f:      l.f,
	}
}

func (l *StandardLogger) Logf(lvl Level, format string, args ...interface{}) {
	if lvl >= l.lvl {
		msg := fmt.Sprintf(format, args...)
		output, err := l.f.Format(lvl, l.fields.Iter(nil), msg)
		if err != nil {
			panic(err)
		}

		l.write(output)
		switch lvl {
		case FatalLevel:
			l.fatalFunc()
		case PanicLevel:
			panic(msg)
		}
	}
}

func (l *StandardLogger) Tracef(format string, args ...interface{}) {
	l.Logf(TraceLevel, format, args...)
}

func (l *StandardLogger) Debugf(format string, args ...interface{}) {
	l.Logf(DebugLevel, format, args...)
}

func (l *StandardLogger) Infof(format string, args ...interface{}) {
	l.Logf(InfoLevel, format, args...)
}

func (l *StandardLogger) Warnf(format string, args ...interface{}) {
	l.Logf(WarnLevel, format, args...)
}

func (l *StandardLogger) Errorf(format string, args ...interface{}) {
	l.Logf(ErrorLevel, format, args...)
}

func (l *StandardLogger) Fatalf(format string, args ...interface{}) {
	l.Logf(FatalLevel, format, args...)
}

func (l *StandardLogger) Panicf(format string, args ...interface{}) {
	l.Logf(PanicLevel, format, args...)
}
