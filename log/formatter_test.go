package log_test

import (
	"bytes"
	"fmt"
	"testing"
	"time"

	"smpuppet/log"
)

func TestTimeFormatter(t *testing.T) {
	formatters := []string{
		"",
		log.DefaultTimeFormat,
		time.RFC3339,
	}

	for _, format := range formatters {
		testFormatter(t, log.NewTimeFormatter(format))
	}
}

func testFormatter(t *testing.T, f log.Formatter) {
	m := log.NewMap()
	m.Set("key1", "value A")
	m.Set("key2", "value B")
	m.Set("another", 42)

	lvl := log.DebugLevel
	msg := "Message"

	suffix := "[key1:value A] [key2:value B] [another:42] Message"

	out, err := f.Format(lvl, m.Iter(nil), msg)
	if err != nil {
		t.Fatalf("Unexpected format error: %v", err)
	}

	expectedSfx := []byte(fmt.Sprintf("%s %s\n", lvl.String(), suffix))
	if out == nil || !bytes.HasSuffix(out, expectedSfx) {
		t.Fatalf("Unexpected format output: "+
			"got %q, expected suffix %q", out, expectedSfx)
	}
}
