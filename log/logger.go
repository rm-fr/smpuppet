package log

// Level represents logging level.
type Level int

const (
	TraceLevel Level = iota - 2
	DebugLevel
	InfoLevel
	WarnLevel
	ErrorLevel
	FatalLevel
	PanicLevel
)

func (l Level) String() string {
	r := "UNKNOWN"
	switch l {
	case TraceLevel:
		r = "TRACE"
	case DebugLevel:
		r = "DEBUG"
	case InfoLevel:
		r = "INFO"
	case WarnLevel:
		r = "WARN"
	case ErrorLevel:
		r = "ERROR"
	case FatalLevel:
		r = "FATAL"
	case PanicLevel:
		r = "PANIC"
	}
	return r
}

// Logger is the interface needed to be supported by a logger
// in order to be used for smpuppet logging.
type Logger interface {
	With(key string, value interface{}) Logger
	Logf(lvl Level, format string, args ...interface{})
	Panicf(format string, args ...interface{})
	Fatalf(format string, args ...interface{})
	Errorf(format string, args ...interface{})
	Warnf(format string, args ...interface{})
	Infof(format string, args ...interface{})
	Debugf(format string, args ...interface{})
	Tracef(format string, args ...interface{})
}
