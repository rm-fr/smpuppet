package log_test

import (
	"bytes"
	"fmt"
	"strings"
	"testing"

	"smpuppet/log"
)

func expectOutput(t *testing.T, actual, expected []byte, isSuffix bool) {
	switch isSuffix {
	case true:
		if !bytes.HasSuffix(actual, expected) {
			t.Fatalf("Unexpected logger output: "+
				"got %q, expected suffix %q", actual, expected)
		}
	case false:
		if !bytes.Equal(actual, expected) {
			t.Fatalf("Unexpected logger output: "+
				"got %q, expected %q", actual, expected)
		}
	}
}

func TestStdLoggerFormatter(t *testing.T) {
	b := new(bytes.Buffer)
	msg := "Test message"

	logger := log.NewStdLogger(log.WithStdOutput(b),
		log.WithStdFormatter(log.NewTimeFormatter("")))

	logger.With("key", "value").Infof(msg)

	expectedOut := []byte(fmt.Sprintf(
		"%s [%s:%s] %s\n", log.InfoLevel, "key", "value", msg))
	expectOutput(t, b.Bytes(), expectedOut, false)
}

type fmter struct {
	//Format (func(log.Level, log.Pairs, string) ([]byte, error))
}

func (f fmter) Format(_ log.Level, _ log.Pairs, _ string) ([]byte, error) {
	return nil, fmt.Errorf("error")
}

func TestStdLoggerFormatterError(t *testing.T) {
	defer func() {
		r := recover()
		if r == nil {
			t.Fatalf("Panic expected")
		}
	}()

	b := new(bytes.Buffer)
	msg := "Test message"

	logger := log.NewStdLogger(log.WithStdOutput(b),
		log.WithStdFormatter(fmter{}))

	logger.Warnf(msg)

	expectOutput(t, b.Bytes(), nil, false)
}

func TestStdLoggerOutput(t *testing.T) {
	b := new(bytes.Buffer)
	msg := "Test message"

	logger := log.NewStdLogger(log.WithStdOutput(b))

	logger.Infof(msg)

	expectedSfx := []byte(fmt.Sprintf("%s %s\n", log.InfoLevel, msg))
	expectOutput(t, b.Bytes(), expectedSfx, true)
}

func TestStdLoggerLevel(t *testing.T) {
	capitalize := func(s string) string {
		return strings.Title(strings.ToLower(s))
	}

	loggerLevel := log.WarnLevel

	for _, l := range []log.Level{
		log.TraceLevel, log.DebugLevel,
		log.InfoLevel, log.WarnLevel, log.ErrorLevel,
		log.FatalLevel, log.PanicLevel} {

		t.Run(capitalize(l.String()), func(t *testing.T) {
			defer func() {
				r := recover()
				if r != nil && l != log.PanicLevel {
					t.Fatalf("Unexpected panic occured: %v", r)
				}
			}()

			b := new(bytes.Buffer)
			logger := log.NewStdLogger(log.WithStdOutput(b),
				log.WithStdLevel(loggerLevel),
				log.WithStdFormatter(log.NewTimeFormatter("")),
				log.WithStdFatalFunc(func() {}))

			msg := "Test message"
			logger.Logf(l, msg)

			expectedOut := []byte(fmt.Sprintf("%s %s\n", l, msg))
			switch l >= loggerLevel {
			case true:
				expectOutput(t, b.Bytes(), expectedOut, false)
			default:
				expectOutput(t, b.Bytes(), nil, false)
			}
		})
	}
}

func TestStdLoggerFmtCalls(t *testing.T) {
	capitalize := func(s string) string {
		return strings.Title(strings.ToLower(s))
	}

	loggerLevel := log.TraceLevel

	for _, c := range []struct {
		lvl  log.Level
		call func(log.Logger) func(string, ...interface{})
	}{
		{
			lvl: log.TraceLevel,
			call: func(l log.Logger) func(string, ...interface{}) {
				return l.Tracef
			},
		},
		{
			lvl: log.DebugLevel,
			call: func(l log.Logger) func(string, ...interface{}) {
				return l.Debugf
			},
		},
		{
			lvl: log.InfoLevel,
			call: func(l log.Logger) func(string, ...interface{}) {
				return l.Infof
			},
		},
		{
			lvl: log.WarnLevel,
			call: func(l log.Logger) func(string, ...interface{}) {
				return l.Warnf
			},
		},
		{
			lvl: log.ErrorLevel,
			call: func(l log.Logger) func(string, ...interface{}) {
				return l.Errorf
			},
		},
		{
			lvl: log.FatalLevel,
			call: func(l log.Logger) func(string, ...interface{}) {
				return l.Fatalf
			},
		},
		{
			lvl: log.PanicLevel,
			call: func(l log.Logger) func(string, ...interface{}) {
				return l.Panicf
			},
		},
	} {

		t.Run(capitalize(c.lvl.String()), func(t *testing.T) {
			defer func() {
				r := recover()
				if r != nil && c.lvl != log.PanicLevel {
					t.Fatalf("Unexpected panic occured: %v", r)
				}
			}()

			b := new(bytes.Buffer)
			logger := log.NewStdLogger(log.WithStdOutput(b),
				log.WithStdLevel(loggerLevel),
				log.WithStdFormatter(log.NewTimeFormatter("")),
				log.WithStdFatalFunc(func() {}))

			msg := "Test message"
			c.call(logger)(msg)

			expectedOut := []byte(fmt.Sprintf("%s %s\n", c.lvl, msg))
			expectOutput(t, b.Bytes(), expectedOut, false)
		})
	}
}
