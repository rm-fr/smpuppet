package log_test

import (
	"testing"

	"smpuppet/log"
)

func expectLen(t *testing.T, m log.Mapper, expected int) {
	if m.Len() != expected {
		t.Fatalf("Map length == %d (!= %d)", m.Len(), expected)
	}
}

func expectOrderedMap(t *testing.T, m log.Mapper,
	expectedAssoc map[string]interface{},
	expectedOrder []string, useOrderInIter bool) {

	i := 0
	var ch <-chan log.Pair
	switch useOrderInIter {
	case true:
		ch = m.Iter(func(_ []string) []string { return expectedOrder })
	default:
		ch = m.Iter(nil)
	}
	for p := range ch {
		if i >= len(expectedOrder) {
			t.Fatalf("Unexpected map element: (key: %q, value: %q)", p.Key, p.Value)
		}

		xKey := expectedOrder[i]
		xVal := expectedAssoc[xKey]
		if p.Key != xKey {
			t.Fatalf("Wrong key order: expected %q, got %q", xKey, p.Key)
		}
		if p.Value != xVal {
			t.Fatalf("Wrong key value: expected %q, got %q", xVal, p.Value)
		}

		i++
	}
}

func TestMapper(t *testing.T) {
	m := log.NewMap()
	expectLen(t, m, 0)

	// Test length with one key
	m.Set("key", "value")
	expectLen(t, m, 1)

	// Test length with two keys
	m.Set("another key", 42)
	expectLen(t, m, 2)

	// Test iteration order
	elements := map[string]interface{}{
		"key":         "value",
		"another key": 42,
	}
	defaultOrder := []string{"key", "another key"}
	expectOrderedMap(t, m, elements, defaultOrder, false)

	// Test length with updated key
	m.Set("key", "new value")
	expectLen(t, m, 2)

	// Test iteration order with updated key
	elements["key"] = "new value"
	updatedOrder := []string{"another key", "key"}
	expectOrderedMap(t, m, elements, updatedOrder, false)

	// Test iteration order with new key
	m.Set("last", "discombobulation")
	newEls := map[string]interface{}{
		"key":         "new value",
		"another key": 42,
		"last":        "discombobulation",
	}
	newOrder := []string{"another key", "key", "last"}
	expectOrderedMap(t, m, newEls, newOrder, false)

	// Test iteration order with filtered keys
	expectOrderedMap(t, m, newEls, []string{"another key"}, true)

	// Test map copy
	newM := m.Copy()
	newM.Set("after copy", "exists only in old map")
	copiedEls := map[string]interface{}{
		"key":         "new value",
		"another key": 42,
		"last":        "discombobulation",
		"after copy":  "exists only in old map",
	}
	copiedOrder := []string{"another key", "key", "last", "after copy"}
	expectOrderedMap(t, newM, copiedEls, copiedOrder, false)

	// Test that the old map is not mutated
	expectOrderedMap(t, m, newEls, newOrder, false)
}
