package log_test

import (
	"bytes"
	"testing"

	"smpuppet/log"
)

func TestLockWriterWrite(t *testing.T) {
	b := new(bytes.Buffer)
	data := []byte("Verification test output")

	lw := log.NewLockWriter(b)
	lw.Write(data)

	output := b.Bytes()
	if !bytes.Equal(output, data) {
		t.Fatalf("Output differs: expected %q got %q", data, output)
	}
}
