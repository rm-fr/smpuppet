package log_test

import (
	"testing"

	"smpuppet/log"
)

func TestLevelString(t *testing.T) {
	for k, v := range map[log.Level]string{
		log.TraceLevel: "TRACE",
		log.DebugLevel: "DEBUG",
		log.InfoLevel:  "INFO",
		log.WarnLevel:  "WARN",
		log.ErrorLevel: "ERROR",
		log.FatalLevel: "FATAL",
		log.PanicLevel: "PANIC",
	} {
		if k.String() != v {
			t.Fatalf("Unexpected level description: "+
				"got %q, expected %q", k.String(), v)
		}
	}
}
