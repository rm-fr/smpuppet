package log

import (
	"io"
	"sync"
)

// LockedWriter is an interface for an io.Writer
// for serializable write access.
type LockedWriter interface {
	sync.Locker
	io.Writer
}

// LockWriter is an implementation of LockedWriter.
type LockWriter struct {
	sync.Mutex

	w io.Writer
}

func (w *LockWriter) Write(b []byte) (int, error) {
	w.Lock()
	defer w.Unlock()
	return w.w.Write(b)
}

func NewLockWriter(w io.Writer) *LockWriter {
	return &LockWriter{w: w}
}
