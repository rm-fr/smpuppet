package server

import (
	"sync"
)

// taskPool is a pool of tasks.
type taskPool struct {
	wg  sync.WaitGroup
	cnt chan uint
}

// newPool initializes and returns a new task pool.
func newPool() *taskPool {
	cnt := make(chan uint, 1)
	cnt <- 0

	return &taskPool{
		cnt: cnt,
	}
}

// Stop stops the pool.
func (p *taskPool) Stop() {
	// TODO: Currently unimplemented
}

// borrow performs task addition accounting.
func (p *taskPool) borrow() {
	count := <-p.cnt
	p.cnt <- count + 1
	p.wg.Add(1)
}

// release performs task removal accounting.
func (p *taskPool) release() {
	count := <-p.cnt
	p.cnt <- count - 1
	p.wg.Done()
}

// Submit adds a task to the pool.
func (p *taskPool) Submit(f func() error) {
	p.borrow()
	go func() {
		defer p.release()
		f()
	}()
}

// count returns the number of tasks in the pool.
func (p *taskPool) count() uint {
	count := <-p.cnt
	p.cnt <- count

	return count
}

// Wait waits until all tasks have terminated.
func (p *taskPool) Wait() {
	p.wg.Wait()
}
