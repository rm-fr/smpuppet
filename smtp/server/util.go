package server

import (
	"fmt"
	"net"
)

// joinAddress returns the network and address as a string.
func joinAddress(addr net.Addr) string {
	return fmt.Sprintf("%s://%s", addr.Network(), addr.String())
}
