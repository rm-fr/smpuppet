package server

import (
	"net"
	"time"

	"smpuppet/log"
)

const (
	connType = "tcp"
)

// ServerState represents the server state.
type ServerState int

const (
	// State_Listen represents that the server
	// is listening but not accepting connections.
	State_Listen ServerState = iota
	// State_Accept represents that the server
	// is operational and serving connections.
	State_Accept
	// State_Deny represents that the server
	// is currently not accepting new connections.
	State_Deny
	// State_Retire represents that the server
	// has retired the task pool.
	State_Retire
)

func (st ServerState) String() string {
	switch st {
	case State_Listen:
		return "Listen"
	case State_Accept:
		return "Accept"
	case State_Deny:
		return "Deny"
	case State_Retire:
		return "Retire"
	default:
		return "Unknown"
	}
}

// ServerConfig is used for server configuration.
type ServerConfig struct {
	// Address is the address to listen on for incoming connections.
	Address string
	// Logger is the server logger.
	// If unset, a standard logger with default options will be used.
	Logger log.Logger
	// Handler is the handler used for serving the connections.
	// If unset, the connections will be logged and closed immediately.
	Handler Handler
	// MaxWorkers is the maximum number of workers
	// allowed to be spawned by the worker pool.
	// If unset, it will be equal to DefaultMaxWorkers.
	//MaxWorkers uint
}

// Server listens for clients and handles the connections.
type Server struct {
	// log is the server logger.
	log log.Logger

	// pool is the task pool manager for the server.
	pool *taskPool
	// clientHandler is the handler function called to service a connection.
	clientHandler Handler

	// listener is the listener the server is responsible for serving.
	listener net.Listener

	// state is used for atomic server state updates.
	state (chan ServerState)
	// closed is used to signal that the server has terminated.
	closed chan bool
}

// getState returns the server state.
func (s *Server) getState() ServerState {
	state := <-s.state
	s.state <- state

	s.log.Tracef("State: %s", state)

	return state
}

// setState sets the server state.
func (s *Server) setState(state ServerState) {
	st := <-s.state
	s.state <- state

	s.log.Infof("State transition: %s -> %s", st, state)
}

// Handler is the type of a connection handler.
type Handler func(net.Conn, log.Logger) error

// New creates a new server with the provided configuration.
// The server does not accept connections yet.
func New(cfg ServerConfig) (*Server, error) {
	var err error

	var logger log.Logger = log.NewStdLogger()
	if cfg.Logger != nil {
		logger = cfg.Logger
	}

	// Listen on provided address
	var listener net.Listener
	if listener, err = net.Listen(connType, cfg.Address); err != nil {
		logger.Errorf("[%s] Unable to install listener: %v",
			cfg.Address, err)
		return nil, err
	}
	logger = logger.With("interface", listener.Addr())
	logger.Infof("Server listening")

	s := &Server{
		log:           logger,
		pool:          newPool(),
		listener:      listener,
		clientHandler: cfg.Handler,
		state:         make(chan ServerState, 1),
		closed:        make(chan bool),
	}
	s.state <- State_Listen

	return s, nil
}

// accept accepts connections from the listener and
// passes them to the worker pool to be serviced.
func (s *Server) accept() {
	s.setState(State_Accept)
	s.log.Infof("Server accepting connections")

	for s.getState() == State_Accept {
		// TODO: Maybe use a saner setup here.
		// When we change server state, the waiting accept
		// will not be notified until the listener is closed...
		conn, err := s.listener.Accept()
		if err != nil {
			if s.getState() > State_Accept {
				s.log.Infof("Server is not accepting connections")
				return
			}
			s.log.Errorf("Failed to accept: %v", err)
			continue
		}

		s.log.Infof("New connection request from "+
			"remote client %s", joinAddress(conn.RemoteAddr()))

		s.pool.Submit(s.wrapHandler(conn, s.log))
	}
	// Wait until termination to return.
	<-s.closed
}

// Serve starts the accept loop, and accordingly
// spawns handlers to handle the connections in a new goroutine.
func (s *Server) Serve() {
	s.accept()
}

// deny stops any more incoming connections from being accepted.
// It does not affect any currently accepted connections.
func (s *Server) deny() {
	s.setState(State_Deny)
}

// Retire stops accepting new connections.
func (s *Server) Retire() {
	s.log.Infof("Declining new connections")
	s.deny()
	s.log.Infof("Retiring worker pool")
	s.pool.Stop()
	s.setState(State_Retire)
}

// Stop stops the server, terminating any currently
// served connections after the provided timeout.
func (s *Server) Stop(grace time.Duration) (err error) {
	s.Retire()

	s.log.Infof("Waiting %s for graceful termination", grace)
	select {
	case <-time.After(grace):
		s.log.Infof("Server terminating forcefully")
		if err = s.listener.Close(); err != nil {
			s.log.Errorf("Listener terminated with error: %v", err)
		}
	}

	s.log.Infof("Waiting for worker pool retirement")
	s.pool.Wait()

	s.log.Infof("Server terminated")
	close(s.closed)
	return nil
}

// wrapHandler returns a wrapper around the provided handler.
// The provided handler should not close the connection.
// By default, the returned wrapper is rude, in that it
// logs the connection, and if the handler is unset, immediately closes it.
func (s *Server) wrapHandler(c net.Conn, l log.Logger) func() error {
	h := s.clientHandler

	return func() error {
		defer func() {
			if err := c.Close(); err != nil {
				l.Warnf("Connection termination error: %v", err)
			}
			l.Infof("Connection terminated by server")
		}()

		var herr error
		if h != nil {
			if herr = h(c, l); herr != nil {
				l.Errorf("Handler terminated with error: %v", herr)
			}
		}
		return herr
	}
}
