package main

import (
	"os"
	"os/signal"
	"syscall"

	guerrilla "github.com/flashmob/go-guerrilla"
	backends "github.com/flashmob/go-guerrilla/backends"
	glog "github.com/flashmob/go-guerrilla/log"
)

const (
	defaultPidFile       = ".guerrilla.pid"
	defaultDomain        = "smpuppet.freeddns.org"
	defaultListenAddress = "0.0.0.0:25"
	defaultMaxSize       = int64(100 << 20)
	defaultTimeout       = 30
	defaultMaxClients    = 100
)

var (
	signalChannel = make(chan os.Signal, 1)
	mainLogger    glog.Logger
	daemon        guerrilla.Daemon
)

func init() {
	var err error

	if mainLogger, err = glog.GetLogger(
		glog.OutputStderr.String(),
		glog.DebugLevel.String(),
	); err != nil {
		mainLogger.WithError(err).Errorf(
			"Failed to create logger to %s", glog.OutputStderr)
	}
}

func newDaemon() guerrilla.Daemon {
	srvCfg := guerrilla.ServerConfig{
		LogFile:         glog.OutputStderr.String(),
		Hostname:        defaultDomain,
		ListenInterface: defaultListenAddress,
		MaxSize:         defaultMaxSize,
		Timeout:         defaultTimeout,
		MaxClients:      defaultMaxClients,
		IsEnabled:       true,
	}
	// https://github.com/flashmob/go-guerrilla/wiki/Backends,-configuring-and-extending
	backendCfg := backends.BackendConfig{
		"primary_mail_host": defaultDomain,
		// Logging options
		"log_received_emails": true,
		// Backend worker pool size
		"save_workers_size": 2,
		// Processor chain for saving mail
		"save_process": "HeadersParser|Header|Hasher|Debugger",
		// Saving conveyor timeout
		"gw_save_timeout": "30s",
		// Processor chain for validating recipients
		// TODO: Write custom validation processor
		"validate_process": "",
		// Validation conveyor timeout
		"gw_val_rcpt_timeout": "5s",
	}
	appCfg := &guerrilla.AppConfig{
		Servers:       []guerrilla.ServerConfig{srvCfg},
		AllowedHosts:  []string{defaultDomain},
		PidFile:       defaultPidFile,
		LogFile:       glog.OutputStderr.String(),
		LogLevel:      glog.DebugLevel.String(),
		BackendConfig: backendCfg,
	}

	daemon := guerrilla.Daemon{
		Config: appCfg,
		Logger: mainLogger,
	}

	return daemon
}

func sigHandler() {
	signal.Notify(signalChannel,
		syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGINT)

	for sig := range signalChannel {
		mainLogger.Debugf("Signal caught: %v", sig)
		switch sig {
		case syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGINT:
			mainLogger.Info("Shutting down...")
			daemon.Shutdown()
			mainLogger.Info("Shutdown complete")
			return
		default:
			mainLogger.Warn("Unknown signal caught. Terminating")
			daemon.Shutdown()
			return
		}
	}
}

func main() {
	daemon = newDaemon()

	if err := daemon.Start(); err != nil {
		mainLogger.WithError(err).Error("Error during server startup")
		os.Exit(1)
	}

	sigHandler()
}
