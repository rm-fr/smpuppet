.PHONY: clean xnayd

DAEMON := xnayd

MODULE_NAME := smpuppet

GO = go

$(DAEMON):
	$(GO) build -i -v -o $(DAEMON) $(MODULE_NAME)/cmd/$(DAEMON)
	sudo setcap 'cap_net_bind_service=+ep' $(DAEMON)
	sudo -K

fmt:
	goimports -e -w -local $(MODULE_NAME) .

tidy:
	$(GO) mod tidy

clean:
	$(RM) $(DAEMON)
